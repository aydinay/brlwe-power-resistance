\vspace{-0.5em}
\subsection{DPA Attack and Countermeasure of B-RLWE}
\label{sec:DPA}

DPA can exploit small power correlations to key by using a statistical analysis on a sufficiently large number of power traces, and break a design that has SPA countermeasure.

\subsubsection{Attacking SPA-secure Executions with DPA}

Due to the accumulative nature of polynomial multiplication, the computation carried out at a certain time depends on the current key bit and all the previous key bits that have been used in the multiplication.
For example, the result of the second row, depends on the first key bit ($r_2[0]$) and the second key bit ($r_2[1]$).
The crux of the DPA attack is to exploit this notion: a different key history will lead to a different computation.
Using this attack, the adversary, starting from the first bit, can subsequently recover the entire secret key. 

\begin{table} [t] 
	\vspace{-1.5em}
	\caption{DPA attack intuition for SPA-secure circuit. Value of $r_2[i-1]$ correlates with power consumption at row $i$}
	\vspace{-1.5em}
	\label{table:DPA}
	\begin{center}
		\vspace{-1em}
	    \setlength{\tabcolsep}{1pt}
		\renewcommand{\arraystretch}{2}
		\begin{tabular} {c||c|c||c|c|c|c}
			\vspace{-1em}
			& \multicolumn{2}{c||}{Attacking first row} & \multicolumn{4}{c}{Attacking second row}\\
			& \multicolumn{2}{c||}{($r_2[0]$)} & \multicolumn{4}{c}{($r_2[1]$,$r_2[0]$)}\\\hline
			$r_2[i]$ & 0 & 1 & 	00 & 01 & 10 & 11 \\\hline
			\parbox{1.3cm}{\centering Adder Operation} & 0 $\rightarrow$ 30 & 0 $\rightarrow$ 30 & \phantom{ll}\marktopleftalt{c1}\textbf{0 $\rightarrow$ 20}\markbottomrightalt{c1}\phantom{ll} & \phantom{l} \marktopleft{c1}\textbf{30 $\rightarrow$ 50}\markbottomright{c1} \phantom{l} & \phantom{ll}\marktopleftalt{c1}\textbf{0 $\rightarrow$ 20}\markbottomrightalt{c1}\phantom{ll} & \phantom{l} \marktopleft{c1}\textbf{30 $\rightarrow$ 50}\markbottomright{c1} \phantom{l} \\
			
%\parbox{1.3cm}{\centering Redundant Datapath} & 0 $\rightarrow$ 30 & 0 $\rightarrow$ 0 & \phantom{l} \marktopleft{c2}\textbf{30 $\rightarrow$ 50}\markbottomright{c2} \phantom{l}& \phantom{ll}\marktopleftalt{c3}\textbf{0 $\rightarrow$ 20}\markbottomrightalt{c3}\phantom{ll} & 30 $\rightarrow$ 30 & 0 $\rightarrow$ 0\\
			
			
		\end{tabular}
	
	\vspace{-2.5em}
	\end{center}
\end{table} 

Table \ref{table:DPA} visualizes the intuition behind the proposed DPA attack, which details an attack to the toy example in Figure \ref{fig:dec_example} with $c_1=[30\,20\,150\,80]$ and $r_2=[1\,0\,1\,1]$. Let the DPA adversary target the computation of $mult[3]$, which denotes the most significant byte of the intermediate sum of polynomial multiplication. 
Recall that, due to the SPA countermeasure, the intermediate sum's accumulation by $c_1[i]$ will occur regardless of the key $r2[i]$ value.

Attacking the first row of computations fails since regardless of the value of $r_2[0]$ the hardware will compute 30=0+30. Therefore, SPA defense will also protect against the attack that targets computations in the first row.
However, by attacking the second row of polynomial multiplication, the adversary can extract the value of the first bit of secret key (ie. $r_2[0]$).
This is possible because the computation that takes place in the second row depends on previous bit due to the accumulative nature of the computation---even though SPA countermeasure makes it independent of the current key value. 
For the example, if $r_2[0]$ is `1', the datapath will compute 50=30+20, else, if $r_2[0]$ is `0', the datapath computes 20=0+20. 
Thus, the adversary can effectively execute a \emph{differential attack}; it can apply a random input to the decryption, make a hypothesis on the intermediate computation from input value and key guesses, and check those hypothesis through the power trace.
This attack will recover the value of each key bit, starting from $r_2[0]$, and it can successively recover consecutive bits of the key.
%\footnote{It is also possible to make a hypothesis on more than a single bit, say $l$-bits, and check correlations at $l+1$-th row}.
For example, after recovering $r_2[0]$, the attacker can target the computations of the third row and extract $r_2[1]$.
When this DPA attack completes; only 2 key candidates remain depending if $r_2[n$$-$$1]$$=$$0$ or $r_2[n$$-$$1]$$=$$1$. The brute force search space will thus reduce from $2^{n}$ to 2 options. 

The proposed DPA attack extracts the secret key by making and testing hypothesis on a single intermediate computation, which is typically referred to as \emph{first-order} DPA attacks.


\begin{figure}[t]
	\vspace{-2em}
	\centering
	\includegraphics[width=0.5\textwidth]{randominit_new.png}
	\vspace{-2em}
	\caption{Example of random initialization with masked threshold decoder.}
	\label{fig:randominit}
	\vspace{-1.5em}
\end{figure}


\subsubsection{Novel DPA countermeasure with Random Initialization and Masking}


We propose a novel first-order DPA countermeasure for B-RLWE decryption based on random initialization and a masked threshold decoder. Our approach is to initialize the intermediate sum with a random value at the start of each decryption process and to recover from the effect of this random value at the final step using a masked threshold decoder. 
Figure \ref{fig:randominit} illustrates this method and shows its effect on the same example used in Figure \ref{fig:dec_example}.
Since decryption operations have additive homomorphism, the initialized random values will be preserved right before the threshold decoder (ie. each coefficient is shifted by a fixed, random amount).
However, simply subtracting the random numbers before the threshold may allow a DPA-adversary to correlate against the output values; DPA defenses remove \emph{all} correlations between intermediate computations and the key.
The masked threshold decoder takes random initialization, shifted result, and a random bit as input for each coefficient to generate two bits $m'$ and $m''$ such that $m=m'\oplus m''$. 
Since it uses a random bit for each new execution, the masked threshold decoder successfully de-correlates the final operation from the secret key.
In practice the masked threshold decoder can be implemented with a $128K\times2$ ROM.
%$F_{mth}=(f_{th}\oplus m,r),r$



Figure \ref{fig:hw_arch} shows the implementation of our DPA defense. We used a PRNG based on TRIVIUM \cite{trivium08} to optimize area, which is also used in previous lightweight lattice-based implementations \cite{popelmann14-2}. Any cryptographically secure PRNG can be used; this selection has \emph{no impact} on our side-channel analysis. PRNG supplies random bits to initialize memory contents that store intermediate sum (both real and redundant memory) and to apply masked threshold decoder. The initialized values are also stored in a memory ($Rand.$ $Init.$, Figure \ref{fig:hw_arch}) to be later used in the masked threshold decoder.

\subsubsection{Comparison of Proposed Countermeasure with Prior Methods} Our DPA countermeasure has a lower execution-time overhead compared to previous masked solutions applied for RLWE \cite{reparaz15}, \cite{oder16}. Both of these schemes perform random splitting: the key is split into two random shares and each share is computed separately, thus doubling the cycle count. 
In contrast, the cycle count overhead of our method is much smaller ($<4\%$), which is the time it takes to generate random numbers to properly initialize the memory. 
We also do not opt for the threshold decoder proposed in \cite{reparaz15} for several reasons. First, it reduces the reliability of decryption. Second, it further increases the execution time. Third, it may be vulnerable to horizontal DPA attacks. Moreover, the operand size of B-RLWE is 8-bits, thus a straightforward threshold decoder with a table lookup occupies relatively smaller area. The DPA-resistance approach of \cite{reparaz16} drastically reduces the reliability because the decryption has to correct the aggregated errors of two encryptions. Moreover, it also enforces a decryption system to include overheads of a full encryption module.
Our method does not have these drawbacks.


\subsubsection{Higher-order DPA Attacks}
\label{sec:higher_order}

Higher-order DPA attacks can break systems that are protected with lower-order masking. We describe a second-order DPA attack to our first-order DPA defense (see Section \ref{sec:second-DPA} for results). 
The goal of this attack is to negate the effect of random initialization by picking two points in the power trace and by combining these two points to make direct correlations to the secret key value.
The proposed attack uses absolute-difference based combination \cite{messerges00}. It essentially exploits the correlation of Hamming weight ($HW$) of absolute differences ($HW(a)$$\sim$$|HW(a\!+\!r$ $mod256)\!-\!HW(r)|$), thereby enables correlating directly to the target value.
To achieve this attack, the adversary correlates against the combination of random numbers generated by the PRNG ($HW(r)$) and to the random intermediate addition $HW(a+r$ $mod256)$.
Therefore, the second-order DPA-adversary applies the first-order DPA attack method on the two combined points.
