\section{Evaluation of Side-channel Attack Resilience}

To evaluate the power attacks on a real environment, we mapped our implementations on the SAKURA-G board, which includes a Xilinx Spartan-6 (XC6SLX75-2CSG484C) FPGA. 
%This is a special platform designed for side-channel testing purposes and thus allows sampling the power consumption of the main crypto FPGA with minimal interference of other components inside the same board.
We measure the voltage drop on a 1-$\Omega$ resistance and make use of the on-board amplifiers on the SAKURA-G platform.
The measurements are taken with a low-end digital USB oscilloscope (PicoScope 2204A) that can sample at 20 ns intervals (50 MS/s). The design is clocked at a constant 1.5 MHz operating frequency.


\begin{figure}[t]
	\vspace{-1.5em}
	\centering
	\includegraphics[width=0.5\textwidth]{spa_results.png}
	\vspace{-2.5em}
	\caption{Difference-of-means based SPA-resistance tests without (a) and with (b) the SPA countermeasure. Figures show the evolution of the difference-of-means at the maximum leak point for 150000 traces. Dashed lines mark the confidence interval for difference-of-means equal to 0 with 99.99\%. In the baseline design (a), starting from 200 traces, the attack can successfully estimate the correct guess with a confidence of 99.99\%. After applying the SPA countermeasure (b), the SPA leak is mitigated even after 150000 measurements. This test is applied on $r_2[2]$ when $r_2[1]$ and $r_2[0]$ is fixed to `1' and `0', respectively.}
	\label{fig:spatest}
	\vspace{-1.5em}
\end{figure}

\subsection{Testing SPA Resilience}
\label{sec:SPA-test}

To evaluate SPA-resistance, we perform a difference-of-means test on the baseline and SPA-resistant design. This test aims to differentiate two sets---measurements taken when a particular key bit is equal to `1' vs `0'---by comparing their means separately for each point in the mean power trace. Figure \ref{fig:spatest} reflects the outcome of this test for up to 150000 measurements. The results validates our SPA countermeasure: At the maximum leak point (ie, at the point where maximum difference occurs in time), while the baseline design shows a large difference in the mean power consumption for two different key values, this effect is mitigated in the SPA-resistant design.



\subsection{Testing DPA Resilience}

To evaluate DPA-resistance, we follow the methodology presented in \cite{reparaz15} that applies a three-pronged test and uses the same Pearson correlation in the DPA attack \cite{brier04}.
We target both the input and output registers of the adders (ie. intermediates before and after addition) and make hypothesis based on the Hamming distance of register values.
Our security model, similar to previous work, assumes that DPA adversary knows the details of hardware architecture like data-flow, parallelization choice, and pipelines in the hardware.
Adversary can therefore estimate when the targeted computation will likely occur and apply the attack around those clock cycles.
We test the capability of a real adversary that has no \emph{a priori} knowledge of the key.
In contrast, previous attack evaluations either assume that adversary knows all key segments except the targeted one \cite{reparaz15}, \cite{reparaz16}, or assesses a lower bound on the potential of risk \cite{oder16} using the non-specific vs. random t-test \cite{schneider15}.


\begin{figure}[t!]
	\vspace{-1.9em}
	\begin{adjustwidth}{-.1cm}{}
	\centering
	\includegraphics[width=0.49\textwidth]{pearson_time_plot_new.png}
	\end{adjustwidth}
	\vspace{-1.2em}
	\caption{First-order DPA attack while the masks turned off. This attack makes hypothesis on intermediate computation $mult[3]$ where $mult=c_1\cdot r_2$. The top trace, depicted in blue, shows the mean power consumption of the FPGA at each evaluation time. In the middle, green trace presents the correlation of correct guess which leaks the value of the key bit $r_2[0]$ at the expected time interval. At the bottom, the red trace displays the correlation of wrong guess; no correlations are observed. Dashed lines mark the confidence interval of $\rho=0$ with 99.99\%.
	}
	\label{fig:pearson_time}
	\vspace{-1.5em}
\end{figure}


\begin{figure*}[t!]
	\centering
	\vspace{-2em}
	\includegraphics[width=1\textwidth]{pearson_eval_all_new.png}
	\vspace{-2.5em}
	\caption{DPA resilience evaluation using (a) first-order attack on the baseline design, (b) first-order attack on the masked design, and (c) second-order attack on the masked design.
	Results show the evolution of the correlation coefficient $\rho$ at the maximum leak point, note the increase in number of measurements for (b) and (c). Dashed lines mark the confidence interval of $\rho=0$ with 99.99\%; DPA attack succeeds if correlation crosses this threshold for the correct key guess. Starting from 2000 traces in (a), DPA estimates the correct key. In (b), even after 150000 traces, DPA fails. In (c), starting from 80000 traces, DPA estimates the correct key.}
	\label{fig:pearson_evol_all}
	\vspace{-1.5em}
\end{figure*}


\subsubsection{DPA with PRNG off}

We first test the effectiveness of first-order DPA attack by turning off the PRNG in the masked architecture. After this deactivation, PRNG keeps a constant output value of `0'. Therefore, the DPA security of the design will be equivalent to an unmasked, baseline implementation. 
%Note that, this will not happen in practice and is just applied for testing the vulnerability of the baseline design to DPA attacks.
Figure \ref{fig:pearson_time} illustrates the result of DPA attack using 15000 traces. Figure shows the first 15 clock cycles of execution during the second row of polynomial multiplication.
DPA attack targets the intermediate value of $mult[3]$; it will make a hypothesis on the fourth coefficient of intermediate sum, based on the value of key bits ($r_2[i\!-\!1]$), starting from $r_2[0]$.
Recall that, as we described in Section \ref{sec:DPA}, the two hypothesis $r_2[i-1]\!=\!0$ and $r_2[i-1]\!=\!1$ will generate different switching activity and DPA attack can check which of these hypothesis correlates with real power measurements.
The selection of $mult[3]$ is not special, the same attack works using any other coefficient, of course, by building accurate intermediate guesses for that target coefficient.
Results validate the DPA vulnerability. 
There is indeed a significant correlation for the correct guess occurring at the expected clock cycle, which is statistically sufficient to extract the correct guess with over 99.99\% confidence.
On the other hand, correlation with incorrect key guess is within the bounds of confidence interval, which demonstrates that, statistically, there is no correlation for the incorrect key guess.



Figure \ref{fig:pearson_evol_all} (a) presents the change in the maximum correlation for both correct guess and wrong guess, which is useful to asses how many traces are needed for DPA to succeed with the target confidence.
After 2000 traces, the correlation of correct key guess exceeds the confidence interval, while incorrect key guess is always within its bounds.
Therefore, this value puts an upper limit on first-order DPA-resistance of the entire system.
The figure reflects the attack to the first two bits and demonstrates its extraction within a window of 15 clock cycles. 
By focusing on other parts of the same power trace (eg. third-row computations) the adversary can successively recover all bits of the key.



\subsubsection{DPA with PRNG on}

After verifying the DPA-vulnerability, we activate the PRNG and repeat the same process. 
%This time, first we also activate an oracle that can predict every random number generated by the PRNG and therefore make accurate hypothesis on the intermediate values as if no PRNG is used. This is again applied to test the effectiveness of the attack and to verify that if randomness of mask can be predicted, the DPA attack will succeed. The results match very closely to the first test and hence omitted for brevity.
%We then test the real first-order DPA adversary, who cannot predict random numbers generated by PRNG.
This attack shall not succeed if the DPA defense is sound.
Since we cannot prove the non-existence of first-order leaks, we empirically validate, by using 150000 traces, if there is a meaningful correlation for both guesses.
Figure \ref{fig:pearson_evol_all} (b) displays the result of this experiment.
As expected, random initialization and the masked threshold decoder prevents first-order DPA attacks.
There is indeed no statistical difference as the correlation of both key guesses converge to 0.




\subsubsection{Second-order DPA with PRNG on}
\label{sec:second-DPA}
Finally, we test the success of our second-order attack---see section \ref{sec:higher_order} for the description of the attack.
This process also verifies if the number of traces used in first-order attack is sufficient.
Figure \ref{fig:pearson_evol_all} (c) shows the result of second-order DPA attack.
The correlation of correct key guess is lower than the first-order DPA thus it requires more traces to break the system with the same confidence.
Therefore, using approximately 80000 traces, which is substantially more than first-order DPA, second-order attacks can still succeed with 99.99\% confidence.
Note that we make a strong assumption on the second-order DPA adversary: it can predict the two fixed points in time causing leaks and combine them.
In practice, first-order defenses are typically implemented with methods that circumvent this prediction such as inserting random idle states, randomizing the order of executions, and adding dummy operations \cite{kocher01}.




%\subsection{Comparison with RLWE DPA}

%A strict comparison of DPA with RLWE is hard to make because of the differences in target device, oscilloscope sampling granularity and precision, and algorithmic/implementation differences of the implementations. 
%The attack of Reparaz \textit{et al.} \cite{reparaz15} can reliably estimate a key after approximately 200 traces. Our attack requires an order-of-magnitude more traces to achieve the same success rate. There are two possible reasons for this outcome. First, in RLWE implementation, two 13-bit values are multiplied generating a 26-bit intermediate value. In contrast, all the intermediate signals in B-RLWE are 8-bits. Therefore, in B-RLWE, there are less registers (less information) to correlate. Second, their setup uses an oscilloscope can sample 10 times faster than our platform and with less noise.

%\subsection{Row-first vs. Column-first Polynomial Multiplication}

%The hardware architectures we implemented follows a row-first polynomial multiplication.
%In terms of hardware cost, we observed negligible variations in row-first vs column-first designs.
%However, this decision may impact the power side-channel analysis.
%For the low-area, coefficient-serial implementations, the SPA and the proposed DPA attacks do not make a difference whether the design is row-serial or column-serial.
%However, for a parallel implementation, a column-first design may hide large variations in power consumption because the activity will depend on the value of multiple key bits. Therefore, such designs may protect against straightforward SPA.
%In contrast, a row-first design will still have no activity when $r_2[i]$ is `0' and will be even more vulnerable to SPA because the variation in power levels will be larger than a serial design as multiple coefficients are either processed or skipped within a clock cycle.

%For the scope of this work, our DPA attack targets a single coefficient. 
%For advanced DPA attacks, like horizontal DPA \cite{clavier10}, the row-wise implementation would be an easier target because all elements in the row are updated based on the value of a single key bit.
%In other words, the DPA adversary can improve the confidence on key-hypothesis by checking the correlation of multiple hypothesis guesses in consecutive clock cycles, which will all individually leak the \emph{same} value of secret-key, and by combining this information. 
%With our parameters, a single allows 256 
