\vspace{-1em}
\section{B-RLWE Public-key Encryption Scheme}

B-RLWE\footnote{To emphasize its \emph{binary} aspect, we propose to change the word order, refer the scheme as binary-ring-learning-with-errors, and abbreviate it as B-RLWE.} is a new, promising variant of RLWE that achieves smaller key sizes and more efficient computations \cite{buchmann16-2}.
The security analysis of B-RLWE is corroborated by several authors \cite{wunderer16,gopfert17,albrecht17}.
This section first provides a background on B-RLWE and introduces the public-key encryption scheme. We then highlight the difference of B-RLWE from RLWE and give a high-level motivation for its impact on power side-channels.

%\subsection{Notation}
All Latin letters such as $r_i$, $c_i$, $c$ express polynomials, unless stated otherwise. The letter $m$ indicates a binary string, and $i$, $n$, and $q$ denotes an integer. The operations $+,-,\cdot$ respectively corresponds to polynomial addition, subtraction, and multiplication, if they operate with polynomials. The symbol $\gets$ shows sampling a polynomial with binary coefficients and $\oplus$ is an XOR operation. We use $c[i]$ to represent a specific coefficient of polynomial $c$ with the degree $i$. To display multiple coefficients of a polynomial $c$ in a single notation, we use $c=[c[i]\, c[i$$-$$1]]$.

\begin{figure}[t!]
	\vspace{-1.5em}
	\centering
	
	%\setlength{\tabcolsep}{1em}
	%\hspace{-1.5cm}	
	
	\begin{adjustwidth}{-.2cm}{}
		\setlength{\tabcolsep}{0pt}
		\begin{tabular} {C{2.7cm}C{6.8cm}}
			
			
			\parbox{2.7cm}{\centering {\tt KeyGen} \\ $r_1,r_2 \gets \mathcal{R}_q$ \\ $p\!=\!r_1\!-\!a\!\cdot\!r_2$} 
			& \parbox{6.8cm}{Key generation samples the secret key $r_2$ and polynomial $r_1$ with binary coefficients, and produces public key $p$. $a$ is a global, public system parameter. $r_1$ can be discarded after this step.}
			\\
			& \\
			\parbox{2.7cm}{\centering
				{\tt Enc}\\ $e_1,e_2,e_3 \gets \mathcal{R}_q$ \\ $c_1\!=\!a\!\cdot\!e_1\!+\!e_2$ \\ $c_2\!=\!p\!\cdot\!e_1\!+\!e_3\!+\!\tilde{m}$}
			& \parbox{6.8cm}{Encryption generates the ciphertext $c_1,c_2$ from message $m$ by using public key $p$ and by adding error nonces $e_1,e_2,e_3$ sampled with binary coefficients. The input message $m$, which is an $n$-bit binary string, is expanded to the polynomial $\tilde{m}$ by multiplying each coefficient with $q/2$ and adding $q+n/2-1-i$, where $i$ is the degree associated with the coefficient.}\\
			& \\
			
			\parbox{2.7cm}{\centering
				{\tt Dec}\\ $m\!=\!th(\!r_2\!\cdot\!c_1\!+\!c_2)$}  & \parbox{6.8cm}{Decryption reconstructs the message $m$ by using the secret key $r_2$. The threshold decoder $th(.)$ processes each coefficient separately and it returns a binary value `1' if $c[i]$ lies in the range ($q/4$, $3q/4$), else it returns `0'.}\\
			
		\end{tabular}
	\end{adjustwidth}
	\caption{B-RLWE public-key encryption scheme}
	\label{fig:B-RLWE_scheme}
	\vspace{-1em}
\end{figure}
%\footnotetext{This addition is implemented during encryption in reference software \cite{buchmann16}, thus we follow it and describe/implement it the same way.}


%\subsection{B-RLWE Algorithm Description}

B-RLWE uses the ring $\mathcal{R}_q$$=$$\mathbb{Z}_q[x]/\langle x^n$$+$$1 \rangle$, where an element inside this ring is a polynomial of degree $n$$-$$1$ with integer coefficients modulo $q$. The operations defined in this ring are polynomial addition, polynomial subtraction, and polynomial multiplication. The polynomial addition and subtraction is simply applying a coefficient-wise, modular addition and subtraction operations, respectively. Polynomial multiplication generates a $2n$$-$$2$ degree polynomial which is converted back to a polynomial of degree $n$$-$$1$ with the reduction function. The reduction function of $f(x)$$=$$x^n$$+$$1$ is chosen to enable an efficient reduction using a polynomial subtraction; the $2n$$-$$2$ degree polynomial is converted to an $n-1$ degree polynomial by subtracting every coefficient $c[i]$ with degree $i$ greater than $n$$-$$1$ from the coefficient with degree $c[i$$-$$n]$.

The B-RLWE public-key encryption scheme consists of three parts: Key Generation ({\tt KeyGen}), Encryption ({\tt Enc}), and Decryption ({\tt Dec}). Note that, while encryption operates with public or ephemeral values, decryption uses the \emph{long-term secret key} and thus is the target of side-channel attacks. Therefore, we implement and analyze the side-channel security of the decryption process. Figure \ref{fig:B-RLWE_scheme} summarizes the B-RLWE scheme.




For the scope of this paper, we use the parameter set-\rom{2} in \cite{buchmann16}, which achieves a relatively lower decoding failure probability of $2^{-32}$, and a security level of 84-bits (88-bits according to Wunderer \cite{wunderer16}) against conventional computers, and a security level of 73-bits \cite{gopfert17} against quantum computers. Since we target \emph{lightweight applications} (eg. constrained IoT nodes) where typical security level is 80-bits (eg. \cite{bogdanov07}), these security levels suffice for our case. The target configuration sets $n=256$ and $q=256$; it works with polynomials degree 255 and with coefficients modulo 256. \emph{The same 8-bit arithmetic we analyze supports 190-bit security level so our side-channel analysis covers B-RLWE instantiations of higher security}. Note that, these parameters are smaller than state-of-the-art RLWE settings \cite{lindner11}, which is favorable for lightweight applications. But these parameters do not, by default, allow an Number Theoretic Transform (NTT) based polynomial multiplication. The parameters, however, may be tweaked to enable NTT for trading off area for performance; such optimizations and their side-channel analysis are out of scope of this work. 

\begin{figure}[t!]
	\vspace{-2em}
	\centering
	\includegraphics[width=0.48\textwidth]{decryption-example_updated.png}
	\vspace{-1em}
	\caption{(a) B-RLWE vs. (b) RLWE decryption example. Note the difference in secret key $r_2$; while B-RLWE works with binary coefficients, RLWE samples them from Gaussian distribution, which, for the example, is between 0 and 255.}
	\vspace{-2em}
	\label{fig:dec_example}
\end{figure}

%\subsection{B-RLWE vs. RLWE Decryption}

Figure \ref{fig:dec_example} illustrates an example of decryption operations for B-RLWE and RLWE.
For simplicity, the example uses the same toy setting  $n=4$ and $q=256$ both for B-RLWE and RLWE. 
The figure reflects the schoolbook polynomial multiplication of $c_1\cdot r_2$ with in-place reduction.
Each row of multiplication shows the product of a single $r_2$ coefficient with all coefficients of $c_1$; we refer to this method as \emph{row-wise} multiplication.
The in-place reduction is simply a sign change (modulo 256) of the reduced coefficient. 
Result of polynomial multiplication is the addition of all row-wise computations.
In practice, this occurs in a \emph{row-first} manner by computing a row of partial products and by adding them to the intermediate sum that holds the result of previous row.
The polynomial multiplication thus has an accumulative nature, which will later in Section {\ref{sec:DPA}} play a key role in DPA attacks.



The main difference of B-RLWE vs. RLWE decryption is that the secret-key polynomial $r_2$ of B-RLWE is sampled with binary coefficients. Hence, polynomial multiplication of B-RLWE is a sequence of additions.
If key bit $r_2[i]$ is equal to `0', intermediate sum keeps its value (third row in the example), else coefficients of $c_1$ are accumulated into the intermediate sum.
Although being more efficient, this property opens up new and destructive side-channels as we discuss next in Section \ref{sec:new_channels}.



%For simplicity, we use a toy example B-RLWE also allows a reduction in the size of the coefficients.


%\section{Hardware Design of B-RLWE}



